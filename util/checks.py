from discord.ext import commands

from util import context


def check_permissions(ctx, perms, *, check=all):
    owner = ctx.author.id == ctx.cfg['athena']['owner']
    if owner:
        return True

    resolved = ctx.channel.permissions_for(ctx.author)
    return check(getattr(resolved, name, None) == value for name, value in perms.items())


def has_permissions(*, check=all, **perms):
    def predicate(ctx):
        return check_permissions(ctx, perms, check=check)

    return commands.check(predicate)


def check_guild_permissions(ctx: context.Context, perms, *, check=all):
    owner = ctx.author.id == ctx.cfg['athena']['owner']
    if owner:
        return True

    if ctx.guild is None:
        return False

    resolved = ctx.author.guild_permissions
    return check(getattr(resolved, name, None) == value for name, value in perms.items())


def has_guild_permissions(*, check=all, **perms):
    def predicate(ctx):
        return check_guild_permissions(ctx, perms, check=check)

    return commands.check(predicate)


# These do not take channel overrides into account

def is_mod():
    def predicate(ctx):
        return check_guild_permissions(ctx, {'manage_guild': True})

    return commands.check(predicate)


def is_admin():
    def predicate(ctx):
        return check_guild_permissions(ctx, {'administrator': True})

    return commands.check(predicate)


def is_owner():
    def predicate(ctx):
        return ctx.author.id == ctx.cfg['athena']['owner']

    return commands.check(predicate)


def mod_or_permissions(**perms):
    perms['manage_guild'] = True

    def predicate(ctx):
        return check_guild_permissions(ctx, perms, check=any)

    return commands.check(predicate)


def admin_or_permissions(**perms):
    perms['administrator'] = True

    def predicate(ctx):
        return check_guild_permissions(ctx, perms, check=any)

    return commands.check(predicate)


def cog_enabled():
    def predicate(ctx: context.Context):
        return ctx.db.get_cog(ctx.guild.id, ctx.command.cog_name)

    return commands.check(predicate)


def is_in_guilds(*guild_ids):
    def predicate(ctx):
        guild = ctx.guild
        if guild is None:
            return False
        return guild.id in guild_ids

    return commands.check(predicate)


# Generic guild checks

def is_app_owner(ctx):
    owner_id = ctx.cfg['athena']['owner']
    if ctx.author.id != owner_id:
        raise NotOwner
    return True


def is_guild_owner(ctx):
    try:
        return is_app_owner(ctx)
    except:
        pass
    if ctx.message.author.id != ctx.message.guild.owner.id:
        raise NotServerOwner
    return True


def is_guild_superadmin(ctx):
    try:
        return is_guild_owner(ctx)
    except:
        pass
    perms = ctx.message.channel.permissions_for(ctx.message.author)
    if perms.administrator:
        return True
    raise NotSuperAdmin


def is_guild_admin(ctx):
    try:
        return is_guild_superadmin(ctx)
    except:
        pass
    perms = ctx.message.channel.permissions_for(ctx.message.author)
    if perms.manage_guild:
        return True
    raise NotAdmin


def is_guild_mod(ctx):
    try:
        return is_guild_admin(ctx)
    except:
        pass
    perms = ctx.message.channel.permissions_for(ctx.message.author)
    if perms.manage_messages:
        return True
    raise NotMod


class NotOwner(commands.CommandError):
    pass


class NotServerOwner(commands.CommandError):
    pass


class NotMod(commands.CommandError):
    pass


class NotAdmin(commands.CommandError):
    pass


class NotSuperAdmin(commands.CommandError):
    pass
