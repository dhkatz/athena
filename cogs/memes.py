import urllib.request
import re
import discord
from discord.ext import commands
from peewee import *
from datetime import datetime, timedelta
from discord.errors import Forbidden

athena_db = SqliteDatabase('data/athena.db')
URL_REGEX = re.compile(
    r'(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:\'".,<>?\xab\xbb\u201c\u201d\u2018\u2019]))')


class Meme(Model):
    message = IntegerField(unique=True)
    channel = IntegerField()
    author = IntegerField()
    votes = IntegerField(default=0)
    posted = BooleanField(default=False)
    archive_id = IntegerField(null=True)
    date = DateTimeField()
    content = TextField(null=True)
    attachment = TextField(null=True)
    attachment_id = IntegerField(null=True)

    class Meta:
        database = athena_db


class Memes:
    """Meme archive"""

    def __init__(self, bot):
        self.bot = bot
        self.feed = self.bot.get_channel(222462913724547072)
        self.archive = self.bot.get_channel(362842783452495877)
        self.up_emoji = 384219281056858112
        self.down_emoji = 384219281350590467
        self.threshold = 4
        self.remove_threshold = -4
        self.setup_memes()
        self.bot.scheduler.add_job(self.clean_memes, 'cron', hour=1)

    @staticmethod
    def setup_memes():
        if 'meme' not in athena_db.get_tables():
            Meme.create_table()

    @staticmethod
    def clean_memes():
        for meme in Meme.select().where(Meme.posted == 0):
            if meme.date < datetime.utcnow() - timedelta(weeks=1):
                meme.delete_instance()

    async def add_meme(self, meme: Meme, message):
        attachment = None
        if message.attachments is not None and len(message.attachments) > 0:
            attachment = message.attachments[0].url
            meme.attachment = attachment
        else:
            matches = URL_REGEX.findall(message.content)
            if len(matches) > 0:
                r = urllib.request.urlopen(matches[0][0])
                if r.headers.get_content_maintype() == 'image':
                    attachment = matches[0][0]

        embed = discord.Embed()
        embed.set_author(name=message.author.name, icon_url=message.author.avatar_url)
        # embed.description = 'Posted on {}'.format(datetime.now().strftime('%a %B %d, %Y'))
        embed.add_field(name='Votes', value=str(meme.votes))
        embed.description = message.content
        if attachment is not None:
            embed.set_image(url=attachment)
        new_message = await self.archive.send(embed=embed)
        meme.archive_id = new_message.id

        # if attachment is not None:
        #     attachment_message = await self.archive.send(attachment)
        #     meme.attachment_id = attachment_message.id

        meme.posted = True
        meme.save()

        self.bot.logger.info(f'Archived meme by {message.author.name} ({message.author.id})')

    async def update_meme(self, meme: Meme, message):
        if meme.votes >= self.threshold and not meme.posted:
            await self.add_meme(meme, message)
        elif meme.votes < self.threshold and meme.posted:
            await self.remove_meme(meme, message)
            return
        elif meme.votes <= self.remove_threshold:
            await self.remove_meme(meme, message)
            return
        elif meme.posted:
            author = self.bot.get_user(meme.author)
            embed = discord.Embed()
            embed.set_author(name=author.name, icon_url=author.avatar_url)
            embed.add_field(name='Votes', value=str(meme.votes))
            cur_message = self.archive.get_message(meme.archive_id)
            await cur_message.edit(embed=embed)
            self.bot.logger.info(f'Updated meme by {author.name} ({author.id})')
        meme.save()

    async def remove_meme(self, meme: Meme, message):
        try:
            if meme.posted:
                archive_message = await self.archive.get_message(meme.archive_id)
                await archive_message.delete()
            else:
                await message.delete()
                meme.delete_instance()
        except Forbidden:
            embed = await self.bot.embed_notify(None, 1, 'Error', 'A meme was removed from the archive!', True)
            await self.archive.send(embed=embed, delete_after=15)
        else:
            if meme.posted:
                meme.posted = False
                embed = await self.bot.embed_notify(None, 2, 'Notice', 'A meme was removed from the archive!', True)
                await self.archive.send(embed=embed, delete_after=15)
            else:
                embed = await self.bot.embed_notify(None, 2, 'Notice', 'Someone\'s shit meme just got deleted!', True)
                await self.feed.send(embed=embed, delete_after=15)
        meme.save()

    async def on_raw_reaction_add(self, emoji: discord.PartialEmoji, message_id, channel_id, user_id):
        """Meme hall of fame, adding votes."""
        if channel_id not in (self.feed.id, self.archive.id):
            return

        if emoji.is_custom_emoji() and emoji.id in [self.up_emoji, self.down_emoji]:
            channel = self.bot.get_channel(channel_id)
            message = await channel.get_message(message_id)

            try:
                meme = Meme.get(
                    (Meme.message == message_id) | (Meme.archive_id == message_id) | (Meme.attachment_id == message_id))
            except DoesNotExist:
                meme = Meme(message=message_id, channel=channel_id, author=message.author.id, votes=0,
                            date=datetime.utcnow())

            if emoji.id == self.up_emoji:
                meme.votes += 1
            elif emoji.id == self.down_emoji:
                meme.votes -= 1

            await self.update_meme(meme, message)

    async def on_raw_reaction_remove(self, emoji: discord.PartialEmoji, message_id, channel_id, user_id):
        """Meme hall of fame, removing votes."""
        if channel_id not in [self.feed.id, self.archive.id]:
            return

        if emoji.is_custom_emoji() and emoji.id in [self.up_emoji, self.down_emoji]:
            channel = self.bot.get_channel(channel_id)
            message = await channel.get_message(message_id)
            try:
                meme = Meme.get(
                    (Meme.message == message_id) | (Meme.archive_id == message_id) | (Meme.attachment_id == message_id))
            except DoesNotExist:  # How could this possibly not exist?
                meme = Meme(message=message_id, channel=channel_id, author=user_id, votes=0,
                            date=datetime.utcnow())

            if emoji.id == self.up_emoji:
                meme.votes -= 1
            elif emoji.id == self.down_emoji:
                meme.votes += 1

            await self.update_meme(meme, message)

    async def on_raw_message_delete(self, message_id, channel_id):
        if channel_id != self.feed.id:
            return

        try:
            meme = Meme.get(
                (Meme.message == message_id) | (Meme.archive_id == message_id) | (Meme.attachment_id == message_id))
        except DoesNotExist:
            meme = None

        if meme is not None:
            if meme.posted:
                message = await self.archive.get_message(meme.archive_id)
                await message.delete()
            meme.delete_instance()

    @commands.group(name='meme')
    @commands.is_owner()
    async def meme(self, ctx):
        """Meme base command"""
        if ctx.invoked_subcommand is None:
            pass

    @meme.command(name='archive')
    @commands.is_owner()
    async def meme_archive(self, ctx, message_id: int, votes: int):
        message = await ctx.channel.get_message(message_id)
        try:
            meme = Meme.get((Meme.message == message_id) | (Meme.archive_id == message_id))
        except DoesNotExist:
            meme = Meme(message=message_id, channel=ctx.channel.id, author=message.author.id, votes=votes,
                        date=datetime.utcnow())

        if meme is not None:
            if not meme.posted:
                meme.votes = votes
                await self.add_meme(meme, message)
            else:
                self.bot.embed_notify(ctx, 1, 'Error', 'Meme is already archived!')


def setup(bot):
    bot.add_cog(Memes(bot))
