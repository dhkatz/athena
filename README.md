Athena
===================

[![forthebadge](http://forthebadge.com/images/badges/made-with-python.svg)](http://forthebadge.com)
[![forthebadge](http://forthebadge.com/images/badges/compatibility-club-penguin.svg)](http://forthebadge.com)

A Discord Bot built for use specifically in my friend's servers. Made with hate and [discord.py](https://github.com/Rapptz/discord.py).
This bot is not designed to be setup by anyone else, but it's design intention is easy to understand.

Commands List (WIP)
-------------------
**Info:** Currently each command is prefixed with a period (.)

### Fun ###

Command and Aliases | Description | Usage
----------------|--------------|-------
`.cat` | Get a random cat picture from random.cat | `.cat`

### Music ###

Command and Aliases | Description | Usage
----------------|--------------|-------
`.join` | Joins a voice channel. | `.join General`
`.pause` | Pauses the currently played song. | `.pause`
`.play` | Plays a song. | `.play https://youtu.be/dQw4w9WgXcQ`
`.playing` | Shows info about the currently played song. | `.playing`
`.resume` | Resumes the currently played song. | `.resume`
`.skip` | Vote to skip a song. The song requester can automatically skip. | `.skip`
`.stop` | Stops playing audio and leaves the voice channel. | `.stop`
`.summon` | Summons the bot to join your voice channel. | `.summon`
`.volume` | Sets the volume of the currently playing song. | `.volume 50`

Requirements
------------

Please see the specific requirements listed for each of the packages listed below.

* Python 3.6+
* discord.py[voice] 1.0.0a0+ (Rewrite)